import React from 'react';
import ReactDOM from 'react-dom';

import './index.css';
import App from './App';
import Home from './components/home/Index';
import Account from './components/account/Index';
import Cart from './components/cart/Index';
import Product from './components/product/Index';
import ProductDetails from './components/productDetails/Index';
import Blog from './components/blog/Blog';
import Details from './components/blog/Details';
import Login from './components/login/Index';
import * as serviceWorker from './serviceWorker';
import wishList from './components/home/wishList';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";


ReactDOM.render(
  <React.StrictMode>
    <Router>
      <App>
          <Switch>
             <Route exact path="/" component={Home} />
             <Route path="/account" component={Account} />
             <Route path="/cart" component={Cart} />
             <Route path="/product" component={Product} />
             <Route path="/product-details/:id" component={ProductDetails} />
             <Route path="/blog/list" component={Blog} />
             <Route path="/blog/detail/:id" component={Details} />
             <Route path="/login" component={Login} />
             <Route path="/wishlist" component={wishList} />
             <Route component={Account} />
          </Switch>
      </App>  
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
