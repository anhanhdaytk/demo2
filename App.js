import React, { Component } from 'react';
import {withRouter} from 'react-router-dom';
import Head from './components/layout/Head';
import Footer from './components/layout/Footer'
import MenuLeft from './components/layout/MenuLeft';
import Slider from './components/layout/Slider';
import {MyContext} from './Mycontext';

class App extends Component{
  constructor(props){
      super(props)
      this.state = {
        login : 'login',
      }
      
     this.handleLogin = this.handleLogin.bind(this);
  }
  // componentDidMount(){
  //   if(localStorage.getItem('login')){
  //     const login = JSON.parse(localStorage.getItem('login'));
  //     this.setState({
  //       login : login
  //     }) 
  //     console.log(login);
      
  //   }
  // }

  handleLogin(a){
    console.log(a);
    
    // const login = a == true ? "Logout" : "Login"
    // this.setState({    
    //   login : login
    // })
    localStorage.setItem('isLogin',JSON.stringify(a))    
  }
  render () {
    let route = this.props.location.pathname
    //console.log(route);
    
    return(
        <MyContext.Provider value={{
          state : this.state,
          handleLogin: this.handleLogin
        }}>
          <Head />
          {route === "/" ? <Slider /> : ''}
          <section>
            <div className = "container">
              <div className = "row">
                {/* {route === "/login" ? '' : <MenuLeft />} */}
                {(route.includes("account") || route.includes("login") || route.includes("cart")) ? '' : <MenuLeft />}
                {/* route.includes("account") kiểm tra xem account có trong route hay ko  */}
                
                <div className = 'col-sm-9 padding-right'>
                  {/* <button onClick= {this.handleLogin}>abc</button> */}
                  {this.props.children}
                </div>
              </div>
            </div>
            </section>
            
            
          <Footer />
        </MyContext.Provider>
    )
  }
}
export default withRouter(App);
