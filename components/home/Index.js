import React, { Component } from 'react';
import {
    Link
  }from "react-router-dom";
  import axios from 'axios';
import wishList from './wishList';
class Index extends Component{
    constructor(props){
        super(props)
        this.state = {
            product : [],
            idWishlist : [],
            cart: {}
           
        }
        this.handleWishlist = this.handleWishlist.bind(this);
        this.handleAddtocart = this.handleAddtocart.bind(this);
    }
    componentDidMount(){
        axios.get('http://api-local.com/api/product')        
        .then(res => {
            console.log( res.data.data);
            this.setState({
                product: res.data.data
            })
        })
        .catch  (error => console.log(error));
    }

    handleWishlist(e){
        console.log(e.target.id);
        let id = e.target.id
        let idWishlist = this.state.idWishlist
        idWishlist.push(id)
        // let index = idWishlist.indexOf(id)
        // console.log(index);
        // if(index > -1){
        //     idWishlist.splice(index, 1);
        // }
       
        console.log(this.state.idWishlist);
        localStorage.setItem('wishlist',JSON.stringify(this.state.idWishlist))
        
    }
    handleAddtocart(e){
        
        let cart = this.state.cart;
        let id = e.target.id;
        let qty = 1;
        
        let flag = true;
        if(localStorage.getItem('cart')){
            cart = JSON.parse(localStorage.getItem('cart'));
        }
        Object.keys(cart).map((key,value) =>{
            // console.log(key);
            if(id == key){
                cart[key] = cart[key] + 1;
                flag = false
            }
            
        })
        if(flag){
            cart[id] = qty;
        }
    
       // console.log(cart);
        localStorage.setItem('cart',JSON.stringify(cart)) 
        
        // kiểm tra trước khi add vào nếu chưa có thì cart bằng rỗng thì add mới còn có rồi thì cart bằng localStorage.getItem('cart') và tăng qty
        
    }

    render (){
        let product = this.state.product
        
        
        return(
            <>
                <div className="features_items">{/*features_items*/}
                    <h2 className="title text-center">Features Items</h2>
                    {Object.keys(product).map((key,value) =>{
                        let image = JSON.parse(product[key].image) 
                        //console.log(image);            
                        
                        return(
                            <div key = {key} className="col-sm-4">
                                <div className="product-image-wrapper">
                                <div className="single-products">
                                    <div className="productinfo text-center">
                                    <img src={"http://api-local.com/upload/user/product/" + product[key].id_user + "/" + image[0]} alt="" />
                                    <h2>{product[key].price}</h2>
                                    <p>{product[key].detail}</p>
                                    <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                                    </div>
                                    <div className="product-overlay">
                                    <div className="overlay-content">
                                        <img src={"http://api-local.com/upload/user/product/" + product[key].id_user + "/" + image[0]} alt="" />
                                        <h2>{product[key].price}</h2>
                                        <p>{product[key].detail}</p>
                                        <Link to="#"  onClick={this.handleAddtocart} id={product[key].id} className="btn btn-default add-to-cart"><i id={product[key].id} className="fa fa-shopping-cart" />Add to cart</Link>
                                    </div>
                                    </div>
                                </div>
                                <div className="choose">
                                    <ul className="nav nav-pills nav-justified">
                                    <li><Link to="#" onClick= {this.handleWishlist} id = {product[key].id}><i className="fa fa-plus-square" />Add to wishlist</Link></li>
                                    <li><Link to={"/product-details/" + product[key].id }><i className="fa fa-plus-square" />More</Link></li>
                                    </ul>
                                </div>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </>
        )
    }
}
export default Index;
