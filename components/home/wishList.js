import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
class wishList extends Component {
    constructor(props){
        super(props)
        this.state = {
            wishList : [],
            idWishlist : []
        }
        this.handleRemovewishlist = this.handleRemovewishlist.bind(this);
    }

    componentDidMount(){
        axios.get('http://api-local.com/api/product/wishlist')
        .then (res => {
            console.log(res.data.data);
            this.setState({
                wishList : res.data.data
            })
        })
        .catch  (error => console.log(error));
        let idWishlist = localStorage.getItem('wishlist') ? localStorage.getItem('wishlist') : {}
        console.log(idWishlist);
        this.setState({
            idWishlist :  idWishlist,
        })
        
        
    }
    handleRemovewishlist(e){
        console.log(e.target.id);
        let id = e.target.id//lấy id sp
        let idWishlist = JSON.parse(this.state.idWishlist)//danh sách id sp
        let index = idWishlist.indexOf(id);
       
        idWishlist.splice(index, 1);
        console.log(idWishlist);
        localStorage.setItem('wishlist',JSON.stringify(idWishlist))
        this.setState({
            idWishlist:localStorage.getItem('wishlist') ,
        })
        
    }
    
    render(){
        let wishList = this.state.wishList
        
        let idWishlist = this.state.idWishlist
        
        if(Object.keys(idWishlist).length > 0){
            wishList = wishList.filter( i => idWishlist.includes(i.id)) // lọc danh sách idwishlist có tồn tại trng wishlist 
            return(
            
                <>
                    <div className="features_items">{/*features_items*/}
                        <h2 className="title text-center">Danh sách yêu thích</h2>
                        {Object.keys(wishList).map((key,value) =>{
                            let image = JSON.parse(wishList[key].image)     
                            // return(
                            //     <img src={"http://api-local.com/upload/user/product/" + image[0]} alt="" />
                            // )                   
                            
                            return(
                                <div key = {key} className="col-sm-4">
                                    <div className="product-image-wrapper">
                                    <div className="single-products">
                                        <div className="productinfo text-center">
                                        <img src={"http://api-local.com/upload/user/product/" + wishList[key].id_user + "/" + image[0]} alt="" />
                                        <h2>{wishList[key].price}</h2>
                                        <p>{wishList[key].detail}</p>
                                        <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                                        </div>
                                        <div className="product-overlay">
                                        <div className="overlay-content">
                                            <img src={"http://api-local.com/upload/user/product/" + wishList[key].id_user + "/" + image[0]} alt="" />
                                            <h2>{wishList[key].price}</h2>
                                            <p>{wishList[key].detail}</p>
                                            <Link to="#" id={1} className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                                        </div>
                                        </div>
                                    </div>
                                    <div className="choose">
                                        <ul className="nav nav-pills nav-justified">
                                        <li><Link href="#" onClick={this.handleRemovewishlist} id={wishList[key].id}><i className="fa fa-plus-square" />Remove wishlist</Link></li>
                                        <li><Link to={"/product-details/" + wishList[key].id }><i className="fa fa-plus-square" />More</Link></li>
                                        </ul>
                                    </div>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </>
            )
        }else{
            return( 
                <div className="features_items">{/*features_items*/}
                    <h2 className="title text-center">Danh sách yêu thích</h2>
                    <h3>Hiện tại không có sản phẩm nào trong WishList</h3>
                </div>   
            )
            
        }
        
        
        
    }
}
export default wishList;