import React, { Component } from 'react';
import {
  Link
}from "react-router-dom";
class Index extends Component {
    render () {
        return (
            <div className="features_items">{/*features_items*/}
            <h2 className="title text-center">Features Items</h2>
            <div className="col-sm-4">
              <div className="product-image-wrapper">
                <div className="single-products">
                  <div className="productinfo text-center">
                    <img src="images/shop/product12.jpg" alt="" />
                    <h2>$56</h2>
                    <p>Easy Polo Black Edition</p>
                    <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                  </div>
                  <div className="product-overlay">
                    <div className="overlay-content">
                      <h2>$56</h2>
                      <p>Easy Polo Black Edition</p>
                      <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                    </div>
                  </div>
                </div>
                <div className="choose">
                  <ul className="nav nav-pills nav-justified">
                    <li><Link to><i className="fa fa-plus-square" />Add to wishlist</Link></li>
                    <li><Link to><i className="fa fa-plus-square" />Add to compare</Link></li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="product-image-wrapper">
                <div className="single-products">
                  <div className="productinfo text-center">
                    <img src="images/shop/product11.jpg" alt="" />
                    <h2>$56</h2>
                    <p>Easy Polo Black Edition</p>
                    <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                  </div>
                  <div className="product-overlay">
                    <div className="overlay-content">
                      <h2>$56</h2>
                      <p>Easy Polo Black Edition</p>
                      <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                    </div>
                  </div>
                </div>
                <div className="choose">
                  <ul className="nav nav-pills nav-justified">
                    <li><Link to><i className="fa fa-plus-square" />Add to wishlist</Link></li>
                    <li><Link to><i className="fa fa-plus-square" />Add to compare</Link></li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="product-image-wrapper">
                <div className="single-products">
                  <div className="productinfo text-center">
                    <img src="images/shop/product10.jpg" alt="" />
                    <h2>$56</h2>
                    <p>Easy Polo Black Edition</p>
                    <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                  </div>
                  <div className="product-overlay">
                    <div className="overlay-content">
                      <h2>$56</h2>
                      <p>Easy Polo Black Edition</p>
                      <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                    </div>
                  </div>
                </div>
                <div className="choose">
                  <ul className="nav nav-pills nav-justified">
                    <li><Link to><i className="fa fa-plus-square" />Add to wishlist</Link></li>
                    <li><Link to><i className="fa fa-plus-square" />Add to compare</Link></li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="product-image-wrapper">
                <div className="single-products">
                  <div className="productinfo text-center">
                    <img src="images/shop/product9.jpg" alt="" />
                    <h2>$56</h2>
                    <p>Easy Polo Black Edition</p>
                    <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                  </div>
                  <div className="product-overlay">
                    <div className="overlay-content">
                      <h2>$56</h2>
                      <p>Easy Polo Black Edition</p>
                      <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                    </div>
                  </div>
                  <img src="images/home/new.png" className="new" alt="" />
                </div>
                <div className="choose">
                  <ul className="nav nav-pills nav-justified">
                    <li><Link to><i className="fa fa-plus-square" />Add to wishlist</Link></li>
                    <li><Link to><i className="fa fa-plus-square" />Add to compare</Link></li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="product-image-wrapper">
                <div className="single-products">
                  <div className="productinfo text-center">
                    <img src="images/shop/product8.jpg" alt="" />
                    <h2>$56</h2>
                    <p>Easy Polo Black Edition</p>
                    <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                  </div>
                  <div className="product-overlay">
                    <div className="overlay-content">
                      <h2>$56</h2>
                      <p>Easy Polo Black Edition</p>
                      <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                    </div>
                  </div>
                  <img src="images/home/sale.png" className="new" alt="" />
                </div>
                <div className="choose">
                  <ul className="nav nav-pills nav-justified">
                    <li><Link to><i className="fa fa-plus-square" />Add to wishlist</Link></li>
                    <li><Link to><i className="fa fa-plus-square" />Add to compare</Link></li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="product-image-wrapper">
                <div className="single-products">
                  <div className="productinfo text-center">
                    <img src="images/shop/product7.jpg" alt="" />
                    <h2>$56</h2>
                    <p>Easy Polo Black Edition</p>
                    <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                  </div>
                  <div className="product-overlay">
                    <div className="overlay-content">
                      <h2>$56</h2>
                      <p>Easy Polo Black Edition</p>
                      <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                    </div>
                  </div>
                </div>
                <div className="choose">
                  <ul className="nav nav-pills nav-justified">
                    <li><Link to><i className="fa fa-plus-square" />Add to wishlist</Link></li>
                    <li><Link to><i className="fa fa-plus-square" />Add to compare</Link></li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="product-image-wrapper">
                <div className="single-products">
                  <div className="productinfo text-center">
                    <img src="images/home/product6.jpg" alt="" />
                    <h2>$56</h2>
                    <p>Easy Polo Black Edition</p>
                    <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                  </div>
                  <div className="product-overlay">
                    <div className="overlay-content">
                      <h2>$56</h2>
                      <p>Easy Polo Black Edition</p>
                      <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                    </div>
                  </div>
                </div>
                <div className="choose">
                  <ul className="nav nav-pills nav-justified">
                    <li><Link to><i className="fa fa-plus-square" />Add to wishlist</Link></li>
                    <li><Link to><i className="fa fa-plus-square" />Add to compare</Link></li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="product-image-wrapper">
                <div className="single-products">
                  <div className="productinfo text-center">
                    <img src="images/home/product5.jpg" alt="" />
                    <h2>$56</h2>
                    <p>Easy Polo Black Edition</p>
                    <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                  </div>
                  <div className="product-overlay">
                    <div className="overlay-content">
                      <h2>$56</h2>
                      <p>Easy Polo Black Edition</p>
                      <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                    </div>
                  </div>
                </div>
                <div className="choose">
                  <ul className="nav nav-pills nav-justified">
                    <li><Link to><i className="fa fa-plus-square" />Add to wishlist</Link></li>
                    <li><Link to><i className="fa fa-plus-square" />Add to compare</Link></li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="product-image-wrapper">
                <div className="single-products">
                  <div className="productinfo text-center">
                    <img src="images/home/product4.jpg" alt="" />
                    <h2>$56</h2>
                    <p>Easy Polo Black Edition</p>
                    <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                  </div>
                  <div className="product-overlay">
                    <div className="overlay-content">
                      <h2>$56</h2>
                      <p>Easy Polo Black Edition</p>
                      <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                    </div>
                  </div>
                </div>
                <div className="choose">
                  <ul className="nav nav-pills nav-justified">
                    <li><Link to><i className="fa fa-plus-square" />Add to wishlist</Link></li>
                    <li><Link to><i className="fa fa-plus-square" />Add to compare</Link></li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="product-image-wrapper">
                <div className="single-products">
                  <div className="productinfo text-center">
                    <img src="images/home/product3.jpg" alt="" />
                    <h2>$56</h2>
                    <p>Easy Polo Black Edition</p>
                    <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                  </div>
                  <div className="product-overlay">
                    <div className="overlay-content">
                      <h2>$56</h2>
                      <p>Easy Polo Black Edition</p>
                      <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                    </div>
                  </div>
                </div>
                <div className="choose">
                  <ul className="nav nav-pills nav-justified">
                    <li><Link to><i className="fa fa-plus-square" />Add to wishlist</Link></li>
                    <li><Link to><i className="fa fa-plus-square" />Add to compare</Link></li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="product-image-wrapper">
                <div className="single-products">
                  <div className="productinfo text-center">
                    <img src="images/home/product2.jpg" alt="" />
                    <h2>$56</h2>
                    <p>Easy Polo Black Edition</p>
                    <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                  </div>
                  <div className="product-overlay">
                    <div className="overlay-content">
                      <h2>$56</h2>
                      <p>Easy Polo Black Edition</p>
                      <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                    </div>
                  </div>
                </div>
                <div className="choose">
                  <ul className="nav nav-pills nav-justified">
                    <li><Link to><i className="fa fa-plus-square" />Add to wishlist</Link></li>
                    <li><Link to><i className="fa fa-plus-square" />Add to compare</Link></li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="product-image-wrapper">
                <div className="single-products">
                  <div className="productinfo text-center">
                    <img src="images/home/product1.jpg" alt="" />
                    <h2>$56</h2>
                    <p>Easy Polo Black Edition</p>
                    <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                  </div>
                  <div className="product-overlay">
                    <div className="overlay-content">
                      <h2>$56</h2>
                      <p>Easy Polo Black Edition</p>
                      <Link to="#" className="btn btn-default add-to-cart"><i className="fa fa-shopping-cart" />Add to cart</Link>
                    </div>
                  </div>
                </div>
                <div className="choose">
                  <ul className="nav nav-pills nav-justified">
                    <li><Link to><i className="fa fa-plus-square" />Add to wishlist</Link></li>
                    <li><Link to><i className="fa fa-plus-square" />Add to compare</Link></li>
                  </ul>
                </div>
              </div>
            </div>
            <ul className="pagination">
              <li className="active"><Link to>1</Link></li>
              <li><Link to>2</Link></li>
              <li><Link to>3</Link></li>
              <li><Link to>»</Link></li>
            </ul>
          </div>
          
        )
    }
}
export default Index;