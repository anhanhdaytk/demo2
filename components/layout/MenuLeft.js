import React, { Component } from 'react';
import {
    Link
  }from "react-router-dom";
class MenuLeft extends Component {
    render () {
        return (
            <>
                <div className="col-sm-3">
                <div className="left-sidebar">
                    <h2>Category</h2>
                    <div className="panel-group category-products" id="accordian">{/*category-productsr*/}
                    <div className="panel panel-default">
                        <div className="panel-heading">
                        <h4 className="panel-title">
                            <Link data-toggle="collapse" data-parent="#accordian" to="#sportswear">
                            <span className="badge pull-right"><i className="fa fa-plus" /></span>
                            Sportswear
                            </Link>
                        </h4>
                        </div>
                        <div id="sportswear" className="panel-collapse collapse">
                        <div className="panel-body">
                            <ul>
                            <li><Link to="#">Nike </Link></li>
                            <li><Link to="#">Under Armour </Link></li>
                            <li><Link to="#">Adidas </Link></li>
                            <li><Link to="#">Puma</Link></li>
                            <li><Link to="#">ASICS </Link></li>
                            </ul>
                        </div>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                        <h4 className="panel-title">
                            <Link data-toggle="collapse" data-parent="#accordian" to="#mens">
                            <span className="badge pull-right"><i className="fa fa-plus" /></span>
                            Mens
                            </Link>
                        </h4>
                        </div>
                        <div id="mens" className="panel-collapse collapse">
                        <div className="panel-body">
                            <ul>
                            <li><Link to="#">Fendi</Link></li>
                            <li><Link to="#">Guess</Link></li>
                            <li><Link to="#">Valentino</Link></li>
                            <li><Link to="#">Dior</Link></li>
                            <li><Link to="#">Versace</Link></li>
                            <li><Link to="#">Armani</Link></li>
                            <li><Link to="#">Prada</Link></li>
                            <li><Link to="#">Dolce and Gabbana</Link></li>
                            <li><Link to="#">Chanel</Link></li>
                            <li><Link to="#">Gucci</Link></li>
                            </ul>
                        </div>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                        <h4 className="panel-title">
                            <Link data-toggle="collapse" data-parent="#accordian" to="#womens">
                            <span className="badge pull-right"><i className="fa fa-plus" /></span>
                            Womens
                            </Link>
                        </h4>
                        </div>
                        <div id="womens" className="panel-collapse collapse">
                        <div className="panel-body">
                            <ul>
                            <li><Link to="#">Fendi</Link></li>
                            <li><Link to="#">Guess</Link></li>
                            <li><Link to="#">Valentino</Link></li>
                            <li><Link to="#">Dior</Link></li>
                            <li><Link to="#">Versace</Link></li>
                            </ul>
                        </div>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                        <h4 className="panel-title"><Link to="#">Kids</Link></h4>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                        <h4 className="panel-title"><Link to="#">Fashion</Link></h4>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                        <h4 className="panel-title"><Link to="#">Households</Link></h4>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                        <h4 className="panel-title"><Link to="#">Interiors</Link></h4>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                        <h4 className="panel-title"><Link to="#">Clothing</Link></h4>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                        <h4 className="panel-title"><Link to="#">Bags</Link></h4>
                        </div>
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                        <h4 className="panel-title"><Link to="#">Shoes</Link></h4>
                        </div>
                    </div>
                    </div>{/*/category-products*/}
                    <div className="brands_products">{/*brands_products*/}
                    <h2>Brands</h2>
                    <div className="brands-name">
                        <ul className="nav nav-pills nav-stacked">
                        <li><Link to="#"> <span className="pull-right">(50)</span>Acne</Link></li>
                        <li><Link to="#"> <span className="pull-right">(56)</span>Grüne Erde</Link></li>
                        <li><Link to="#"> <span className="pull-right">(27)</span>Albiro</Link></li>
                        <li><Link to="#"> <span className="pull-right">(32)</span>Ronhill</Link></li>
                        <li><Link to="#"> <span className="pull-right">(5)</span>Oddmolly</Link></li>
                        <li><Link to="#"> <span className="pull-right">(9)</span>Boudestijn</Link></li>
                        <li><Link to="#"> <span className="pull-right">(4)</span>Rösch creative culture</Link></li>
                        </ul>
                    </div>
                    </div>{/*/brands_products*/}
                    <div className="price-range">{/*price-range*/}
                    <h2>Price Range</h2>
                    <div className="well text-center">
                        <div className="slider slider-horizontal" style={{width: '167px'}}><div className="slider-track"><div className="slider-selection" style={{left: '41.6667%', width: '33.3333%'}} /><div className="slider-handle round left-round" style={{left: '41.6667%'}} /><div className="slider-handle round" style={{left: '75%'}} /></div><div className="tooltip top" style={{top: '-30px', left: '63.8333px'}}><div className="tooltip-arrow" /><div className="tooltip-inner">250 : 450</div></div><input type="text" className="span2" defaultValue data-slider-min={0} data-slider-max={600} data-slider-step={5} data-slider-value="[250,450]" id="sl2" style={{}} /></div><br />
                        <b className="pull-left">$ 0</b> <b className="pull-right">$ 600</b>
                    </div>
                    </div>{/*/price-range*/}
                    <div className="shipping text-center">{/*shipping*/}
                        <img src="images/home/shipping.jpg" alt=""/>
                    </div>{/*/shipping*/}
                </div>
                </div>

            </>
        )
    }
}
export default MenuLeft;