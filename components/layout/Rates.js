import React, { Component } from 'react';
import StarRatings from './react-star-ratings';
class Rates extends Component{
    constructor(props){
      super(props)
      this.state={
        rating : 1
      };
      this.changeRating = this.changeRating.bind(this)
    }
    changeRating( nextValue, newRating, name ) {
      this.setState({
        rating: nextValue
      });
    }        
   
    render(){
      const { rating } = this.state
      
      return(
        <div>
        <h2>Rating from state: {rating}</h2>
        <StarRatings 
        disabled={false}
          name="rate1" 
          starCount={10}
          value={rating}
          starRatedColor="blue"
          changeRating={this.changeRating}
          numberOfStars={6}

        />
      </div>
      )  
        
        
    }
}
export default Rates;