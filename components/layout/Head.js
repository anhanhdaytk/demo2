
import React, { Component } from 'react';
import {MyContext} from '../../Mycontext';
import { Link } from 'react-router-dom';
import {withRouter} from 'react-router-dom';
import wishList from '../home/wishList';

// import {
//   Link
// }from "react-router-dom";

class Head extends Component {
    static contextType = MyContext;
    constructor(props){
        super(props)
        this.state = {
            // isLoggedIn : '',
            isLogin: '',
            
        }
        this.isLogin = this.isLogin.bind(this);
        this.handleLogout = this.handleLogout.bind(this);
        this.handleQtyCart = this.handleQtyCart.bind(this);
        this.handleQtyWishlist = this.handleQtyWishlist.bind(this);
    }
    
    
    handleQtyWishlist(){
        let wishlist = JSON.parse(localStorage.getItem('wishlist'))
        if(wishlist){
            console.log(wishlist.length);
            return(
                <Link to="/wishlist"><i className="fa fa-star" /> Wishlist <b>{wishlist.length}</b></Link>
            )
        }else{
            return(
                <Link to="/wishlist"><i className="fa fa-star" /> Wishlist <b>0</b></Link>
            )
        }
    }

    handleQtyCart(){
        let cart = JSON.parse(localStorage.getItem('cart'))
        
        if(cart){
            let cartLength = Object.keys(cart).length
            return(
                <Link to="/cart"><i className="fa fa-shopping-cart" /> Cart <b>{cartLength}</b></Link>
            )
        }else{
            return(
                <Link to="/cart"><i className="fa fa-shopping-cart" /> Cart <b>0</b></Link>
            )
        }
        
    }

    handleLogout(){
        localStorage.removeItem('appState');
        this.context.handleLogin(false);
        alert('Bạn đã đăng xuất');
        this.props.history.push('/login');
    }
    isLogin() {
        let isLogin = JSON.parse(localStorage.getItem('isLogin'))

        if(isLogin===true){
           return (
                <a  onClick={this.handleLogout}><i className="fa fa-lock" />Logout</a>
           )  
        }else{
            return (
                <Link to="/login"><i className="fa fa-lock" />Login</Link>
            ) 
        }
    }
    // componentDidMount() {
    //     let isLogin = JSON.parse(localStorage.getItem('isLogin'))
    //     //console.log(isLogin)
    //     if(isLogin){
    //         this.setState({
    //             isLogin : <Link to="/login"><i className="fa fa-lock" />Logout</Link>
    //         })
            
            
    //     }else{
    //         this.setState({
    //             isLogin : <Link to="/login"><i className="fa fa-lock" />Login</Link>
    //         })
           
    //     }
    // }
    // handleSubmit(e){
    //     e.preventDefault();

    //     const userData = localStorage["appState"] ? JSON.parse(localStorage["appState"]) : {}
    //      console.log(userData);
        
    //     if(Object.keys(userData).length == 0) {
    //      this.setState({
    //          isLoggedIn : 'Login'
    //      })
    //    } else{ 
    //         this.setState({
    //             isLoggedIn : 'Logout'
    //         })
    //     }
    // }
    render () {
        
        //console.log(this.state.isLoggedIn);
        // const login = JSON.parse(localStorage["login"])
        return(
            <div>
                <div className="header_top">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-6">
                                <div className="contactinfo">
                                    <ul className="nav nav-pills">
                                        <li><Link to="/"><i className="fa fa-phone" /> +2 95 01 88 821</Link></li>
                                        <li><Link to="/"><i className="fa fa-envelope" /> info@domain.com</Link></li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-sm-6">
                                <div className="social-icons pull-right">
                                    <ul className="nav navbar-nav">
                                        <li><Link to="/"><i className="fa fa-facebook" /></Link></li>
                                        <li><Link to="/"><i className="fa fa-twitter" /></Link></li>
                                        <li><Link to="/"><i className="fa fa-Linkedin" /></Link></li>
                                        <li><Link to="/"><i className="fa fa-dribbble" /></Link></li>
                                        <li><Link to="/"><i className="fa fa-google-plus" /></Link></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="header-middle">{/*header-middle*/}
                    <div className="container">
                        <div className="row">
                            <div className="col-md-4 clearfix">
                                <div className="logo pull-left">
                                    <Link to="/index.html"><img src="images/home/logo.png" alt="" /></Link>
                                </div>
                            <div className="btn-group pull-right clearfix">
                                <div className="btn-group">
                                    <button type="button" className="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                                        USA
                                        <span className="caret" />
                                    </button>
                                    <ul className="dropdown-menu">
                                        <li><Link to="/">Canada</Link></li>
                                        <li><Link to="/">UK</Link></li>
                                    </ul>
                                </div>
                                <div className="btn-group">
                                    <button type="button" className="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                                        DOLLAR
                                        <span className="caret" />
                                    </button>
                                    <ul className="dropdown-menu">
                                        <li><Link to="/">Canadian Dollar</Link></li>
                                        <li><Link to="/">Pound</Link></li>
                                    </ul>
                                </div>
                            </div>
                            </div>
                            <div className="col-md-8 clearfix">
                                <div className="shop-menu clearfix pull-right">
                                {/* <MyContext.Consumer>
                                    {(context) =>(  */}
                                        
                                        <ul className="nav navbar-nav">
                                            <li><Link to="/account/member"><i className="fa fa-user" /> Account</Link></li>
                                            <li>{this.handleQtyWishlist()}</li>
                                            <li><Link to="/checkout.html"><i className="fa fa-crosshairs" /> Checkout</Link></li>
                                            <li>{this.handleQtyCart()}</li>
                                            <li>{this.isLogin()}</li>
                                            
                                        {/* <li><Link to="/login"><i className="fa fa-lock" onClick = {this.handleSubmit} />{this.state.isLoggedIn}</Link></li> */}
                                        </ul>
                                    {/* )}
                                </MyContext.Consumer> */}
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>{/*/header-middle*/}
                <div className="header-bottom">{/*header-bottom*/}
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-9">
                                <div className="navbar-header">
                                    <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span className="sr-only">Toggle navigation</span>
                                        <span className="icon-bar" />
                                        <span className="icon-bar" />
                                        <span className="icon-bar" />
                                    </button>
                                </div>
                                <div className="mainmenu pull-left">
                                    <ul className="nav navbar-nav collapse navbar-collapse">
                                        <li><Link to="/" className="active">Home</Link></li>
                                        <li className="dropdown"><Link to="/">Shop<i className="fa fa-angle-down" /></Link>
                                            <ul role="menu" className="sub-menu">
                                                <li><Link to="/product">Products</Link></li>
                                                <li><Link to="/product-details">Product Details</Link></li> 
                                                <li><Link to="/checkout.html">Checkout</Link></li> 
                                                <li><Link to="/cart.html">Cart</Link></li> 
                                                <li><Link to="/login.html">Login</Link></li> 
                                            </ul>
                                        </li> 
                                        <li className="dropdown"><Link to="/">Blog<i className="fa fa-angle-down" /></Link>
                                            <ul role="menu" className="sub-menu">
                                                <li><Link to="/blog/list">Blog List</Link></li>
                                                <li><Link to="/blog-single.html">Blog Single</Link></li>
                                            </ul>
                                        </li> 
                                        <li><Link to="/404.html">404</Link></li>
                                        <li><Link to="/contact-us.html">Contact</Link></li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-sm-3">
                                <div className="search_box pull-right">
                                    <input type="text" placeholder="Search" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>{/*/header-bottom*/}
            </div>    
        )
            

    }
}
export default withRouter(Head);