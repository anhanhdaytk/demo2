import React, { Component } from 'react';
import axios from 'axios';
import Formerrors from '../error/Formerrors';
import Signup from './Signup';
import {MyContext} from '../../Mycontext';

class Index extends Component {
    static contextType = MyContext;
    constructor(props){
        super(props)
        this.state = {
            email: '',
            password: '',
            formErrors: {}
        }
        this.handleInput = this.handleInput.bind(this);
        this.handleSubmitLogin = this.handleSubmitLogin.bind(this);
    }
    handleInput (e){
        const nameInput = e.target.name; //email
        
        const value = e.target.value;
        //console.log(e.target);   
        this.setState({
            [nameInput] : value
           
            
        })
        //console.log(nameInput);
    }
    handleSubmitLogin (e){
        e.preventDefault();
        let {email, password, formErrors} = this.state;
        formErrors.name = '';
        let flag = true;
        if(email === ''){
            formErrors.email = "Vui long nhap email";
            flag = false;
        }
        if(password === ''){
            formErrors.password = "vui long nhap password";
            flag = false;
        }
        if(!flag){
            this.setState({
                formErrors: formErrors
            })
        }else{
            const data ={
                email: this.state.email,
                password: this.state.password,
                
            };
            axios.post('http://api-local.com/api/login', data)
            .then(res => {
                
                if(res.data.errors) {
                    this.setState({
                    formErrors : res.data.errors
                })
                }else {
                    let userData = {
                        auth_token: res.data.success.token,
                        auth: res.data.Auth
                      };
                    console.log(userData);
                    let appState = {
                        isLoggedIn: true,
                        user: userData
                    };
                    console.log(appState);
                    
                    this.context.handleLogin(true);
                    // truyền context qua app
                    
                      // save app state with user date in local storage
                    localStorage.setItem('appState', JSON.stringify(appState));
                    // localStorage.setItem('rate', JSON.stringify(res.data))
                    alert('Bạn đã đăng nhập thành công')
                    this.props.history.push('/')
                }
               
            })
        }
        
    }
    // handleUserInputFile (e){
    //     console.log('abc',1111)
    //     const file = e.target.files;
    //     console.log(file)
    //     // send file to api server
    //     let reader = new FileReader();
    //     reader.onload = (e) => {
    //         this.setState({
    //             avatar: e.target.result,
    //             file: file[0]
    //         })
    //     };
    //     reader.readAsDataURL(file[0]);
    //   }
    
    
    
  
    // if(file && file.name !== ''){
    //       let type = file.type.toLowerCase();
    //       let typeArr = type.split('/');
    //       let regex = ["png", "jpg", "jpeg"];
    
    //       if(file.size > 208292) {
    //         flag = false;
    //         errorSubmit.avatar = "is invalid size";
    //       } else if(!regex.includes(typeArr[1])) {
    //         flag = false;
    //         errorSubmit.avatar = "is invalid type image";
    //       }
    //     }
    // const data = {
    //         name: this.state.name,  
    //         email: this.state.email,
    //         password: this.state.password,
    //         phone: this.state.phone,
    //         address: this.state.address,
    //         country: this.state.country,
    //         avatar: this.state.avatar,
    //         level: 0
    //       }
    //       API.post('register', data)
    //       .then(response => {
    //          if(response.data.errors) {
    //             this.setState({
    //                 formErrors: response.data.errors
    //             })

    render(){
        return(
            <section id="form">{/*form*/}
            
                <div className="container">
                    <div className="row">
                    <div className="col-sm-4 col-sm-offset-1">
                        <div className="login-form">{/*login form*/}
                        <h2>Login to your account</h2>
                        <Formerrors formErrors ={this.state.formErrors}/>
                        <form onSubmit={this.handleSubmitLogin}>
                            
                            <input name="email" type="email" placeholder="Email Address" onChange={this.handleInput} value={this.state.email} required  />
                            <input name="password" type="password" placeholder="password" onChange={this.handleInput} value={this.state.password} required  autoComplete="on" />
                            <span>
                            <input type="checkbox" className="checkbox" autoComplete="on"/> 
                            Keep me signed in
                            </span>
                            <button type="submit" className="btn btn-default">Login</button>
                        </form>
                        </div>{/*/login form*/}
                    </div>
                    <div className="col-sm-1">
                        <h2 className="or">OR</h2>
                    </div>
                    <Signup />
                    {/*/sign up form*/}

                    </div>
                </div>
              
            </section>

        )
    }
}
export default Index;