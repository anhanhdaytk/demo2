import React, { Component } from 'react';
import axios from 'axios';
import Formerrors from '../error/Formerrors';
class Signup extends Component{
    constructor(props){
        super(props)
        this.state = {
            email: '',
            name: '',
            password:'',
            address:'',
            phone:'',
            avatar: '',
            country:'',
            formErrors: {}
        }
        this.handleInput = this.handleInput.bind(this);
        this.handleSubmitSignup = this.handleSubmitSignup.bind(this);
        this.handleUserInputFile =this.handleUserInputFile.bind(this);
    }
    handleInput (e){
        const nameInput = e.target.name; //email
       
        const value = e.target.value;
        //console.log(e.target);   
        this.setState({
            [nameInput] : value
        })
    }
    handleSubmitSignup (e){
        e.preventDefault();
        let { name, email, password, address, phone, formErrors, country, file} = this.state;
        formErrors.name = '';
        let flag = true;
        if(name === '') {
           
            formErrors.name = "vui long nhap name";
            flag = false;
        }
        if(email === ''){
            formErrors.email = "Vui long nhap email";
            flag = false;
        }
        if(password === ''){
            formErrors.password = "vui long nhap password";
            flag = false;
        }
        if(address === ''){
            formErrors.address = "vui long nhap address";
            flag = false;
        }
        if(phone === ''){
            formErrors.phone = "vui long nhap phone";
            flag = false;
        }
        if(country === ''){
            formErrors.country = "vui long nhap country";
            flag = false;
        }
        if(file && file.name !== ''){
            let type = file.type.toLowerCase();
            let typeArr = type.split('/');
            let regex = ["png", "jpg", "jpeg"];
           
            console.log(typeArr)
            if(file.size > 2028292) {
              flag = false;
              formErrors.avatar = "is invalid size";
            } 
            else if(!regex.includes(typeArr[1])) {
              flag = false;
              formErrors.avatar = "is invalid type image";
            }
            
        }    
       
    
    
        if(!flag){
            this.setState({
                formErrors: formErrors
            })
        } else {
           // console.log(email)
            const data = {
                name: this.state.name,
                password: this.state.password,
                email: this.state.email,
                address: this.state.address,
                phone: this.state.phone,
                country: this.state.country,
                avatar: this.state.avatar,
                level: 0

            };
            axios.post('http://api-local.com/api/register', data)
            .then(res => {

                console.log(res);
                
                if(res.data.errors) {
                    this.setState({
                    formErrors : res.data.errors
                })
                }else {
                    
                    alert('Bạn đã đăng ký thành công')
                }
               
            })
            
        }
        
    }
    handleUserInputFile (e){
            console.log('abc',1111)
            const file = e.target.files;
          
            console.log(file)
            // send file to api server
            let reader = new FileReader();
            reader.onload = (e) => {
                this.setState({
                    avatar: e.target.result,
                    file: file[0]
                })
            };
            reader.readAsDataURL(file[0]);
        }
          

    render () {
        return(
            <div className="col-sm-4">
                <div className="signup-form">{/*sign up form*/}
                <h2>New User Signup!</h2>
                <Formerrors formErrors ={this.state.formErrors}/>
                <form onSubmit={this.handleSubmitSignup}>
                    <input name="name" type="text" placeholder="Name" onChange={this.handleInput} value={this.state.name}/>
                    <input name="email" type="email" placeholder="Email Address" onChange={this.handleInput} value={this.state.email}/>
                    <input name="password" type="password" placeholder="Password" onChange={this.handleInput} value={this.state.password} autoComplete="on"/>
                    <input name="address" type="text" placeholder="Address" onChange={this.handleInput} value={this.state.address}/>
                    <input name="phone" type="number" placeholder="Phone" onChange={this.handleInput} value={this.state.phone}/>
                    <input name="country" type="text" placeholder="Country" onChange={this.handleInput} value={this.state.country}/>
                    <input name="avatar" type="file" placeholder="Avatar" onChange={this.handleUserInputFile} files={this.state.avatar}/>
                    <button type="submit" className="btn btn-default">Signup</button>
                </form>
                </div>{/*/sign up form*/}
            </div>
        )
    }
}
export default Signup;