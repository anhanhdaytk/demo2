import React, { Component } from 'react';
import axios from 'axios';
import {
    PopupboxManager,
    PopupboxContainer
  } from 'react-popupbox';
import "react-popupbox/dist/react-popupbox.css"
import {
    Link
  }from "react-router-dom";
class Index extends Component {
    constructor(props){
        super(props)
        this.state ={
            productDetais: [],
            images: [],
            imageShow: '',
            
        }
        this.handleClickImage = this.handleClickImage.bind(this);
        this.handleShowClickImage = this.handleShowClickImage.bind(this);
        this.openPopupbox = this.openPopupbox.bind(this)
       
    }

    openPopupbox() {
        const content = <div>{this.handleShowClickImage()}</div>
        PopupboxManager.open({
          content,
          config: {
            titleBar: {
              enable: true,
            },
            fadeIn: true,
            fadeInSpeed: 500
          }
        })
    }
    componentDidMount(){
        
        axios.get('http://api-local.com/api/product/detail/' + this.props.match.params.id)
        .then(res =>{
            
            this.setState({
                productDetais: res.data.data,
                images : JSON.parse(res.data.data.image)
            })
            
        })
        .catch  (error => console.log(error));
        

    }
    
    handleShowClickImage(){
        let imageShow = this.state.imageShow
        let productDetais = this.state.productDetais
        let images = this.state.images
        if (imageShow == '') {
            return(
                <img src={"http://api-local.com/upload/user/product/" + productDetais.id_user + "/" +  "larger_" + images[0]} alt="" />
            )
        }else{
            return(
                <img src={"http://api-local.com/upload/user/product/" + productDetais.id_user + "/" +  "larger_" + imageShow} alt="" />
            )
        } 
    }

    handleClickImage(e){
        console.log(e.target.id);
        this.setState({
            imageShow: e.target.id
        })
    }
   
    render () {
        let productDetais = this.state.productDetais
        let images = this.state.images
        
        return (    
            <>
                <div className="product-details">{/*product-details*/}
                    <div className="col-sm-5">
                        <div className="view-product"> 
                        
                            {this.handleShowClickImage()} 
                              
                            <Link to="#" onClick={this.openPopupbox} rel="prettyPhoto"><h3>ZOOM</h3></Link>
                            <PopupboxContainer />
                             
                        </div>
                        <div id="similar-product" className="carousel slide" data-ride="carousel">
                        {/* Wrapper for slides */}
                        <div className="carousel-inner">
                            {Object.keys(images).map((key,value) => {
                                return(
                                    <div key={key} className="item active">                          
                                        <Link to onClick={this.handleClickImage} ><img className="imageProductDetails"  src={"http://api-local.com/upload/user/product/" + productDetais.id_user + "/" + "small_" + images[key]} id={images[key]} alt="" /></Link>
                                    </div>
                                )
                            })}
                            {Object.keys(images).map((key,value) => {
                                return(
                                    <div className="item">
                                        <Link to><img  src={"http://api-local.com/upload/user/product/" + productDetais.id_user + "/" + "small_" + images[key]} alt="" /></Link>
                                    </div>
                                )
                            })}
                            {Object.keys(images).map((key,value) => {
                                return(
                                    <div className="item">
                                        <Link to><img  src={"http://api-local.com/upload/user/product/" + productDetais.id_user + "/" + "small_" + images[key]} alt="" /></Link>
                                    </div>
                                )
                            })}
                        </div>
                        {/* Controls */}
                        <Link className="left item-control" to="#" data-slide="prev">
                            <i className="fa fa-angle-left" />
                        </Link>
                        <Link className="right item-control" to="#" data-slide="next">
                            <i className="fa fa-angle-right" />
                        </Link>
                        </div>
                    </div>
                    <div className="col-sm-7">
                        <div className="product-information">{/*/product-information*/}
                        <img src="images/product-details/new.jpg" className="newarrival" alt="" />
                        <h2>{productDetais.detail}</h2>
                        <p>Web ID: {productDetais.id}</p>
                        <img src="images/product-details/rating.png" alt="" />
                        <span>
                            <span>{productDetais.price}</span>
                            <label>Quantity:</label>
                            <input type="text" defaultValue={1} />
                            <button type="button" className="btn btn-fefault cart">
                            <i className="fa fa-shopping-cart" />
                            Add to cart
                            </button>
                        </span>
                        <p><b>Availability:</b> In Stock</p>
                        <p><b>Condition:</b> New</p>
                        <p><b>Brand:</b> E-SHOPPER</p>
                        <Link to><img src="images/product-details/share.png" className="share img-responsive" alt="" /></Link>
                        </div>{/*/product-information*/}
                    </div>
                </div>
                {/* <!--/product-details--> */}

                
            </>
        )
    }
}
export default Index;