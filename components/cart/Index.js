import React, { Component } from 'react';

import axios from 'axios';
class Cart extends Component{
    constructor(props){
        super(props)
        this.state = {
            cart : {},
            cartProduct : [],
            
           
        }
        this.handleUpQty = this.handleUpQty.bind(this);
        this.handleDowQty = this.handleDowQty.bind(this);
        
        this.handleRemove = this.handleRemove.bind(this);
    }

    componentDidMount(){
        let cart = JSON.parse(localStorage.getItem('cart'))

        this.setState({
            cart : cart
        })
        
        axios.post('http://api-local.com/api/product/cart', cart)
        .then(res =>{
            
            this.setState({
                cartProduct : res.data.data
            })
            console.log(this.state.cartProduct);  
        })
        .catch  (error => console.log(error));
        
    }
    
    handleUpQty(e){
        let cart = this.state.cart
        let id = e.target.id
        // let qty = this.state.qty
        // console.log(qty);
        
        Object.keys(cart).map((key,value) =>{
            
            if(id == key){
                
                cart[key] = cart[key] + 1;
                
            }
            
        })
        
        console.log(this.state.cart);    
        localStorage.setItem('cart',JSON.stringify(cart)) 

        this.setState({
            cart : cart
        })
        
        axios.post('http://api-local.com/api/product/cart', cart)
        .then(res =>{
            
            this.setState({
                cartProduct : res.data.data
            })
            console.log(this.state.cartProduct);  
        })
        .catch  (error => console.log(error));
        
    }

    handleDowQty(e){
        let cart = this.state.cart
        let id = e.target.id
        // let qty = this.state.qty
        // console.log(qty);
        
       
        Object.keys(cart).map((key,value) =>{
            
            if(id == key & cart[key] > 1 ){
                
                cart[key] = cart[key] - 1;
                
            }
            
        })
        
        console.log(this.state.cart);    
        localStorage.setItem('cart',JSON.stringify(cart)) 

        this.setState({
            cart : cart
        })
        
        axios.post('http://api-local.com/api/product/cart', cart)
        .then(res =>{
            
            this.setState({
                cartProduct : res.data.data
            })
            console.log(this.state.cartProduct);  
        })
        .catch  (error => console.log(error));
        
    }

    

    handleRemove(e){
       
       let id = e.target.id
       let cart = this.state.cart
       delete cart[id]
       console.log(e.target.id); 
       console.log(JSON.stringify(cart));
        
        this.setState({
            cart : cart
        })
        
        localStorage.setItem('cart',JSON.stringify(cart))

        axios.post('http://api-local.com/api/product/cart', cart)
        .then(res =>{
            
            this.setState({
                cartProduct : res.data.data
            })
            console.log(this.state.cartProduct);  
        })
        .catch  (error => console.log(error));

    }

    render (){
        let cartProduct = this.state.cartProduct
        
        let total = 0
        //console.log(total);
        
        return(
            <>
                <section id="cart_items">
                    <div className="container">
                        <div className="breadcrumbs">
                        <ol className="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li className="active">Shopping Cart</li>
                        </ol>
                        </div>
                        <div className="table-responsive cart_info">
                            <table className="table table-condensed">
                                <thead>
                                    <tr className="cart_menu">
                                        <td className="image">Item</td>
                                        <td className="description" />
                                        <td className="price">Price</td>
                                        <td className="quantity">Quantity</td>
                                        <td className="total">Total</td>
                                        <td />
                                    </tr>
                                </thead>
                                <tbody>
                                    {Object.keys(cartProduct).map((key,value) =>{
                                        let image = JSON.parse(cartProduct[key].image) 
                                        total = total + (cartProduct[key].price * cartProduct[key].qty)
                                                                                                                        
                                        //console.log(this.state.total);
                                        
                                        return(
                                            <tr>
                                                <td className="cart_product">
                                                    <a href><img src={"http://api-local.com/upload/user/product/" + cartProduct[key].id_user + "/" + "small_" + image[0]} alt="" /></a>
                                                </td>
                                                <td className="cart_description">
                                                    <h4><a href>{cartProduct[key].detail}</a></h4>
                                                    <p className="id">ID:{cartProduct[key].id}</p>
                                                </td>
                                                <td className="cart_price">
                                                    <p className="price">{cartProduct[key].price}</p>
                                                </td>
                                                <td className="cart_quantity">
                                                    <div className="cart_quantity_button">
                                                        <a onClick={this.handleUpQty} id={cartProduct[key].id} className="cart_quantity_up" > + </a>
                                                        <input className="cart_quantity_input"  type="text" name="quantity"  value={cartProduct[key].qty} autoComplete="off" size={2} />
                                                        <a onClick={this.handleDowQty} id={cartProduct[key].id} className="cart_quantity_down"> - </a>
                                                    </div>
                                                </td>
                                                <td className="cart_total">
                                                    <p className="cart_total_price">{cartProduct[key].price * cartProduct[key].qty}</p>
                                                </td>
                                                <td className="cart_delete">
                                                    
                                                    <a  className="cart_quantity_delete" onClick={this.handleRemove} id={cartProduct[key].id}><i id={cartProduct[key].id}  className="fa fa-times" /></a>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
                {/*  end cart_items */}

                <section id="do_action">
                    <div className="container">
                        <div className="heading">
                        <h3>What would you like to do next?</h3>
                        <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
                        </div>
                        <div className="row">
                        <div className="col-sm-6">
                            <div className="chose_area">
                            <ul className="user_option">
                                <li>
                                <input type="checkbox" />
                                <label>Use Coupon Code</label>
                                </li>
                                <li>
                                <input type="checkbox" />
                                <label>Use Gift Voucher</label>
                                </li>
                                <li>
                                <input type="checkbox" />
                                <label>Estimate Shipping &amp; Taxes</label>
                                </li>
                            </ul>
                            <ul className="user_info">
                                <li className="single_field">
                                <label>Country:</label>
                                <select>
                                    <option>United States</option>
                                    <option>Bangladesh</option>
                                    <option>UK</option>
                                    <option>India</option>
                                    <option>Pakistan</option>
                                    <option>Ucrane</option>
                                    <option>Canada</option>
                                    <option>Dubai</option>
                                </select>
                                </li>
                                <li className="single_field">
                                <label>Region / State:</label>
                                <select>
                                    <option>Select</option>
                                    <option>Dhaka</option>
                                    <option>London</option>
                                    <option>Dillih</option>
                                    <option>Lahore</option>
                                    <option>Alaska</option>
                                    <option>Canada</option>
                                    <option>Dubai</option>
                                </select>
                                </li>
                                <li className="single_field zip-field">
                                <label>Zip Code:</label>
                                <input type="text" />
                                </li>
                            </ul>
                            <a className="btn btn-default update" href>Get Quotes</a>
                            <a className="btn btn-default check_out" href>Continue</a>
                            </div>
                        </div>
                        <div className="col-sm-6">
                            <div className="total_area">
                            <ul>
                                <li>Cart Sub Total <span>$59</span></li>
                                <li>Eco Tax <span>$2</span></li>
                                <li>Shipping Cost <span>Free</span></li>
                                <li>Total <span><b className="total">{total}</b></span></li>
                            </ul>
                            <a className="btn btn-default update" href>Update</a>
                            <a className="btn btn-default check_out" href>Check Out</a>
                            </div>
                        </div>
                        </div>
                    </div>
                </section>
                {/* end do_action */}
            </>
        )
    }
}
export default Cart;