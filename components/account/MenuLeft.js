import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class MenuLeft extends Component{
    render(){
        return(
            <div className="col-sm-3">
            <div className="left-sidebar">
                <h2>Category</h2>
                <div className="panel-group category-products" id="accordian">{/*category-productsr*/}
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h4 className="panel-title">
                                <Link data-toggle="collapse" data-parent="#accordian" to="/account/member" >
                                <span className="badge pull-right"><i className="fa fa-plus" /></span>
                                ACCOUNT
                                </Link>
                            </h4>
                        </div>
                        <div id="sportswear" className="panel-collapse collapse" />
                    </div>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h4 className="panel-title">
                                <Link data-toggle="collapse" data-parent="#accordian" to="/account/product/list">
                                <span className="badge pull-right"><i className="fa fa-plus" /></span>
                                MY PRODUCT
                                </Link>
                            </h4>
                        </div>
                        {/* <div id="sportswear" className="panel-collapse in" style={{height: 'auto'}}>
                            <div className="panel-body">
                                <ul>
                                <li><a>Add </a></li>
                                <li><a href>Under Armour </a></li>
                                <li><a href>Adidas </a></li>
                                <li><a href>Puma</a></li>
                                <li><a href>ASICS </a></li>
                                </ul>
                            </div>
                        </div> */}
                    </div>   
                </div>{/*/category-products*/}
            </div>
            </div>

        )
    }
}
export default MenuLeft;