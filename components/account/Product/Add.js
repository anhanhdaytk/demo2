import React, { Component } from 'react';
import Formerrors from '../../error/Formerrors';
import axios from 'axios';
import { withRouter } from 'react-router-dom';
class Add extends Component{
    constructor(props){
        super(props)
        this.state ={
            category : '',
            brand: '',
            name: '',
            file: [],
            price: '',
            status: '',
            sale: '',
            detail: '',
            company: '',
            formErrors: {},
            listCategory:'', 
            listBrand: '',
        }
        this.handleInput = this.handleInput.bind(this);
        this.handleCategory = this.handleCategory.bind(this);
        this.handleBrand = this.handleBrand.bind(this);
        this.handleStatusSale = this.handleStatusSale.bind(this);
        this.handleSale = this.handleSale.bind(this);
        this.handleProductInputFile = this.handleProductInputFile.bind(this);
        this.handleSubmitAdd = this.handleSubmitAdd.bind(this);
    }
    

    
    componentDidMount(){
        axios.get('http://api-local.com/api/category-brand')
            .then(res =>{
                console.log(res);
                
                this.setState({
                    listCategory: res.data.category, //de lay all category
                    listBrand: res.data.brand,
                });
            })
            .catch  (error => console.log(error));

    }

    handleInput(e){
        const nameInput = e.target.name;
        const value = e.target.value;
        //console.log(value);
        
        this.setState({
            [nameInput] : value,
        })
    }

    handleCategory(){
        
        let listCategory = this.state.listCategory
        //console.log(listCategory);
        return Object.keys(listCategory).map((key,value)=>{
            
            return(
                <option  value={listCategory[key]['id']}>{listCategory[key]['category']}</option>         
            )
        })
        
    }

    handleBrand(){
        let listBrand = this.state.listBrand
        //console.log(listBrand);
        return Object.keys(listBrand).map((key,value)=>{

            return(
                <option value={listBrand[key]['id']}>{listBrand[key]['brand']}</option>         
            )
        })
    }

    handleStatusSale(e){  
        this.setState({
            status: e.target.value,
        })  
    }

    handleSale(){
        let status = this.state.status
        //console.log(status);
        
        if(status == 2){ 
            return(
                <input name="sale" type="text" placeholder="0%" onChange={this.handleInput} value={this.state.sale}/> 
            )
        }
    }

    handleProductInputFile (e){
        
        const fileImage = e.target.files; //lấy ảnh từ input files
        //console.log(fileImage);
            this.setState({
                file: fileImage, //cập nhật dữ liệu file của state
            })
           
    }

    handleSubmitAdd(e){
        e.preventDefault();
       
        let {category, brand, name, file, price, status, detail, company,formErrors} = this.state;
        formErrors.name = formErrors.category =  formErrors.brand = formErrors.file = formErrors.price = formErrors.status = formErrors.detail = formErrors.company ='';
        
        console.log(this.state.sale);
        let flag = true;
        if(name == '') {
            formErrors.name = "vui long nhap name";
            flag = false;
        }
        if(price == ''){
            formErrors.price = "Vui long nhap price";
            flag = false;
        }
        if(status == ''){
            formErrors.status = "vui long chon status";
            flag = false;
        }
        if(category == ''){
            formErrors.category = "vui long nhap category";
            flag = false;
        }
        if(brand == ''){
            formErrors.brand = "vui long nhap brand";
            flag = false;
        }
        if(detail == ''){
            formErrors.detail = "vui long nhap detail";
            flag = false;
        }
        if(company == ''){
            formErrors.company = "vui long nhap company";
            flag = false;
        }
        if(file){
            if(Object.keys(file).length > 3) {
                flag = false;
                formErrors.file = "tối đa 3 hình";
                alert('tối đa 3 hình')
            } else {
                Object.keys(file).map((key,i)=>{
                    
                    let type = file[key].type.toLowerCase();
                    let typeArr = type.split('/');
                    let regex = ["png", "jpg", "jpeg"];   
                    if(!regex.includes(typeArr[1])) {
                        flag = false;
                        formErrors.file = "sai định dạng";
                        alert('sai định dạng')
                    }   
                })
            }   
        }

        if(!flag){
            this.setState({
                formErrors: formErrors
            })
        } else {
            const userData = localStorage["appState"] ? JSON.parse(localStorage["appState"]) : {}
            //console.log(userData);
            
            if(Object.keys(userData).length === 0) {
                alert('vui lòng đăng nhập')
            } else {
                let accessToken = userData.user.auth_token; 

                let config = { 
                    headers: { 
                    'Authorization': 'Bearer '+ accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                    } 
                };
                
                let formData = new FormData();	
                    formData.append('name', this.state.name);	
                    formData.append('category', this.state.category);	
                    formData.append('brand', this.state.brand);	
                    formData.append('price', this.state.price);	
                    formData.append('status',  this.state.status);	
                    formData.append('sale',this.state.sale ? this.state.sale : 0);
                    formData.append('detail', this.state.detail);
                    formData.append('company', this.state.company);
                    
                    Object.keys(file).map((item, i) => {
                        formData.append("file[]", file[item]);
                    });
                let url = 'http://api-local.com/api/user/add-product';

                axios.post(url, formData, config)	
                .then(res => {	    
                    //console.log(res);
                    // this.setState({
                    //     name:'',
                    //     price:'',
                    //     status:'',
                    //     sale:'',
                    //     detail:'',
                    //     company:'',
                    //     file:[]
                    // })
                    if(res.data.errors) {
                        this.setState({
                            formErrors : res.data.errors
                        })
                    }else {
                        alert('thêm sản phẩm thành công')
                    }
                })
                .catch(error => {		
                    console.log(error)		
                })
                this.props.history.push('/account/product/list');
            }
        }
    }
      
    render(){
        // let id_category = this.state.id_category
        // let id_brand = this.state.id_brand
        // let inputSale = this.state.inputSale
        // console.log(id_brand);
        // return Object.keys(id_category).map((key,value)=>{})
        return(
           
                <div className="col-sm-8">
                    <div className="signup-form">
                        <h2>Create Product!</h2>
                        <Formerrors formErrors ={this.state.formErrors}/>
                        {/* <Formerrors formErrors ={this.state.formErrors}/> */}
                        <form onSubmit={this.handleSubmitAdd}>
                            <input name="name" type="text" placeholder="Name" onChange={this.handleInput} value={this.state.name}/>
                            <input name="price" type="number" placeholder="Price"onChange={this.handleInput} value={this.state.price}/>
                            <select name="category" className="form-control" onChange={this.handleInput} value={this.state.category}>
                                <option value="">vui lòng chọn category</option>    
                                {this.handleCategory()}
                            </select>
                            <select name="brand" className="form-control" onChange={this.handleInput} value={this.state.brand}>
                                <option value="">vui lòng chọn brand</option>  
                                {this.handleBrand()}
                            </select>
                            <select name="status" className="form-control" onChange={this.handleStatusSale} value={this.state.status}>
                                <option value="0">Normal</option>
                                <option value="1">New</option>
                                <option value="2">Sale</option>
                            </select>

                            {this.handleSale()}
                
                            <input name="company" type="text" placeholder="Company Profile" onChange={this.handleInput} value={this.state.company}/>
                            <input name="file" type="file" onChange={this.handleProductInputFile} files={this.state.file} multiple/>
                            <textarea  name="detail" type="text" placeholder="Detail" rows={11} onChange={this.handleInput} value={this.state.detail}/> 
                            <button type="submit" className="btn btn-default">Signup</button>
                        </form>
                    </div>{/*/sign up form*/}
                </div>
            
        )
    }
}
export default withRouter(Add);