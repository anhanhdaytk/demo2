import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
class List extends Component{
    constructor(props){
        super(props)
        this.state ={
            listProducts : [],
            idProduct:''
        }
        this.handleListProduct = this.handleListProduct.bind(this);
        this.handleDeleteProduct = this.handleDeleteProduct.bind(this);
    }
    componentDidMount(){
        const userData = localStorage["appState"] ? JSON.parse(localStorage["appState"]) : {}
        console.log(userData);
        if(Object.keys(userData).length === 0) {
            alert('vui lòng đăng nhập')
        } else {
            let accessToken = userData.user.auth_token; 

            let config = { 
                headers: { 
                'Authorization': 'Bearer '+ accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
                } 
            };
            let url = 'http://api-local.com/api/user/my-product';
            axios.get(url, config)
            .then(res => {
    
                console.log(res.data.data);
                this.setState({
                    listProducts : res.data.data
                })
            })
            .catch  (error => console.log(error));
        }
    }
    
    handleListProduct(){

        let listProducts = this.state.listProducts
        
        if(listProducts){
            return Object.keys(listProducts).map((key,value) =>{  
                return (
                    
                    <tr key={key}>
                        <td>
                            <p className="id">{listProducts[key].id}</p>
                        </td>
                        <td>
                            <h4>{listProducts[key].name}</h4>
                        </td>  
                                    
                        <td>
                            <img className="imageProduct" src={"http://api-local.com/upload/user/product/" +listProducts[key].id_user+"/"+ JSON.parse(listProducts[key].image)} alt="" />
                            {
                                Object.keys(JSON.parse(listProducts[key].image)).map((key2,i2) =>{
                                    //console.log(Object.keys(JSON.parse(listProducts[key].image)).length);
                                    if(Object.keys(JSON.parse(listProducts[key].image)).length > 1){
                                        return (
                                            <img key={key2} className="imageProduct" src={"http://api-local.com/upload/user/product/" +listProducts[key].id_user+"/"+ JSON.parse(listProducts[key].image)[key2]} alt="" />
                                        )
                                    }
                                    
                                })
                            }      
                        </td>
                        <td>
                            <p className="price">{listProducts[key].price}</p>
                        </td>
                        <td>
                            <Link  to={"/account/product/Edit/"+ listProducts[key].id} ><i className="fa fa-pencil-square-o" /></Link>
                        </td>
                        <td>
                            <a  className="cart_quantity_delete" onClick={this.handleDeleteProduct}><i id={listProducts[key].id} className="fa fa-times" /></a>
                        </td>
                    </tr>
                
                )
            })
        }
    }
    //end show list product

    handleDeleteProduct(id){     
        id.preventDefault();
        console.log(id.target.id);
        
        const userData = localStorage["appState"] ? JSON.parse(localStorage["appState"]) : {}
        if(Object.keys(userData).length === 0) {
            alert('vui lòng đăng nhập')
        } else {
            let accessToken = userData.user.auth_token; 

            let config = { 
                headers: { 
                'Authorization': 'Bearer '+ accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
                } 
            };
            
            let url = 'http://api-local.com/api/user/delete-product/'+ id.target.id;
            console.log(url);
            
            axios.get(url, config)
            .then(res => {
                console.log(res.data.data);
                
                this.setState({
                    listProducts : res.data.data
                })
                
            })
            .catch  (error => console.log(error));      
        }   
    }
    //end DeleteProduct

   
    render(){
        
        return(
            <div className="row">
                <div  className = "col-sm-3">
                    <section id="cart_items">
                        <div> 
                            <div className="table-responsive cart_info">
                                <table className="table">
                                <thead>
                                    <tr className="cart_menu">
                                    <td className="col-xs-1">Id</td>
                                    <td className="col-xs-2">Name</td>
                                    <td className="col-xs-4, center">Image</td>
                                    <td className="col-xs-2">Price</td>
                                    <td className="col-xs-2">Action</td>
                                    <td className="col-xs-2"/>
                                    </tr>
                                </thead>
                                    <tbody>
                                        {this.handleListProduct()}
                                    </tbody>
                                </table>
                                
                            </div>
                            <Link className="btn btn-primary " to="/account/product/add">Add New</Link>
                        </div>
                    </section>
                </div>
            </div>
        )
    }
}
export default List;