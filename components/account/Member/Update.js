import React,{ Component } from 'react';
import axios from 'axios';
import Formerrors from '../../error/Formerrors';

class Update extends Component{
    constructor(props){
        super(props)
        this.state = {
            email: '',
            name: '',
            password:'',
            address:'',
            phone:'',
            avatar: '',
            country:'',
            formErrors: {},
        }
        // const userData = localStorage["appState"] ? JSON.parse(localStorage["appState"]) : {}
        // this.state = {
        //     email: userData.user.auth.email,
        //     name: userData.user.auth.name,
        //     password:'',
        //     address: userData.user.auth.password,
        //     phone: userData.user.auth.phone,
        //     avatar: userData.user.auth.avatar,
        //     country: userData.user.auth.country,
        //     formErrors: {},
        // }
        this.handleInput = this.handleInput.bind(this);
        this.handleSubmitSignup = this.handleSubmitSignup.bind(this);
        this.handleUserInputFile =this.handleUserInputFile.bind(this);
    }
    
    componentDidMount(){
        const userData = localStorage["appState"] ? JSON.parse(localStorage["appState"]) : {}
        if(Object.keys(userData).length === 0){
            alert('bạn chưa đăng nhập')
        }else{
            this.setState({
                email: userData.user.auth.email,
                name: userData.user.auth.name,
                address: userData.user.auth.address,
                phone: userData.user.auth.phone,
                avatar: userData.user.auth.avatar,
                country: userData.user.auth.country,
                formErrors: {},
            })
        }
    }
    handleInput (e){  
        const nameInput = e.target.name; //lấy name của input đang thao tác
        const value = e.target.value; //lấy value của input 
        //console.log(e.target);  
        
         
        this.setState({
            [nameInput] : value,
        })
    }

    handleUserInputFile (e){
        //console.log('abc',1111)
        const file = e.target.files;
      
        console.log(file)
        // send file to api server
        let reader = new FileReader(); // gán hình vào FileReader để mã hóa hình ảnh
        reader.onload = (e) => {
            console.log(file[0].name);
            
            this.setState({
                avatar: e.target.result,
                file: file[0]
            })
        };
        reader.readAsDataURL(file[0]);
    }

    handleSubmitSignup (e){
        e.preventDefault();
        
   
    
        let { name, email, password, address, phone, formErrors, country, file} = this.state;
        
        formErrors.name = '';
        let flag = true;
        if(name === '') {
           
            formErrors.name = "vui long nhap name";
            flag = false;
        }
        if(email === ''){
            formErrors.email = "Vui long nhap email";
            flag = false;
        }
        if(password === ''){
            formErrors.password = "vui long nhap password";
            flag = false;
        }
        if(address === ''){
            formErrors.address = "vui long nhap address";
            flag = false;
        }
        if(phone === ''){
            formErrors.phone = "vui long nhap phone";
            flag = false;
        }
        if(country === ''){
            formErrors.country = "vui long nhap country";
            flag = false;
        }
        if(file && file.name !== ''){
            let type = file.type.toLowerCase();
            let typeArr = type.split('/');
            let regex = ["png", "jpg", "jpeg"];
           
            //console.log(typeArr)
            if(file.size > 2028292) {
              flag = false;
              formErrors.avatar = "is invalid size";
            } 
            else if(!regex.includes(typeArr[1])) {
              flag = false;
              formErrors.avatar = "is invalid type image";
            }
            
        }    
       
    
    
        if(!flag){
            this.setState({
                formErrors: formErrors
            })
        } else {
           // console.log(email)
            const userData = localStorage["appState"] ? JSON.parse(localStorage["appState"]) : {}
            console.log(userData);
            if(Object.keys(userData).length === 0) {
                alert('vui lòng đăng nhập')
            } else {
                let accessToken = userData.user.auth_token; 

                // console.log(userData.user.auth.id);
                // console.log(userData.user.auth.name);
                // console.log(userData.user.auth.phone);
                // console.log(userData.user.auth.address);
                // console.log(userData.user.auth.country);
                // console.log(userData.user.auth.avatar);

                let config = { 
                    headers: { 
                    'Authorization': 'Bearer '+ accessToken,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'application/json'
                    } 
                };
                
                let formData = new FormData();	
                    formData.append('name', this.state.name);	
                    formData.append('email', this.state.email);	
                    formData.append('password', this.state.password);	
                    formData.append('phone', this.state.phone);	
                    formData.append('address',  this.state.address);	
                    formData.append('country',this.state.country);
                    formData.append('avatar', this.state.avatar);

                let url = 'http://api-local.com/api/user/update/' + userData.user.auth.id;

                axios.post(url, formData, config)	
                .then(res => {	
                    
                    console.log(res);
                    if(res.data.errors) {
                        this.setState({
                            formErrors : res.data.errors
                        })
                    }else {
                        this.setState ({
                            name : userData.user.auth.name
                        })
                        alert('Bạn đã lưu sửa đổi thành công')
                    }
                })
                .catch(error => {		
                    console.log(error)		
                })
            }
        }
        
    }     

    render () {
        return(
            <div className="col-sm-4">
                <div className="signup-form">{/*sign up form*/}
                <h2>New User Signup!</h2>
                <Formerrors formErrors ={this.state.formErrors}/>
                <form onSubmit={this.handleSubmitSignup}>
                    <input name="name" type="text" placeholder="Name" onChange={this.handleInput} value={this.state.name}/>
                    <input name="email" type="email" placeholder="Email Address" onChange={this.handleInput} value={this.state.email}/>
                    <input name="password" type="password" placeholder="Password" onChange={this.handleInput} value={this.state.password} autoComplete="on"/>
                    <input name="address" type="text" placeholder="Address" onChange={this.handleInput} value={this.state.address}/>
                    <input name="phone" type="number" placeholder="Phone" onChange={this.handleInput} value={this.state.phone}/>
                    <input name="country" type="text" placeholder="Country" onChange={this.handleInput} value={this.state.country} />
                    <input name="avatar" type="file" placeholder="Avatar" onChange={this.handleUserInputFile} files={this.state.avatar}/>
                    <button type="submit" className="btn btn-default">Save Edit</button>
                </form>
                </div>{/*/sign up form*/}
            </div>
        )
    }
}
export default Update;
