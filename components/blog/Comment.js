import React, { Component } from 'react';
import axios from 'axios';

class Comment extends Component{
    constructor(props){
        super(props)
            this.state = {
                comments: ""
            }
        this.handleComment = this.handleComment.bind(this);
        this.handleSubmitComment = this.handleSubmitComment.bind(this);  
    }
   
    handleComment(e){
        const value = e.target.value; //lấy giá trị từ thẻ textarea
       // console.log(value);
        this.setState({
            comments : value
        })
        // this.props.loadComment(e.target.value);
    }

    handleSubmitComment(e){
        e.preventDefault();
        const userData = localStorage["appState"] ? JSON.parse(localStorage["appState"]) : {}
        console.log(userData);
       if(Object.keys(userData).length === 0) {
            alert('vui lòng đăng nhập')
       } else {
        let accessToken = userData.user.auth_token; 
        //let isLoggedIn = userData.isLoggedIn; 
        
        
        console.log(userData.user.auth.id);
        console.log(this.props.idBlog);
        console.log(userData.user.auth.name);
        console.log(userData.user.auth.avatar); 
        console.log( this.props.idSubComment);
            let config = { 
                headers: { 
                'Authorization': 'Bearer '+ accessToken,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
                } 
            };
            
            let formData = new FormData();	
                formData.append('id_blog', this.props.idBlog);	
                formData.append('id_user', userData.user.auth.id);	
                formData.append('id_comment', this.props.idSubComment ? this.props.idSubComment : 0);	
                formData.append('comment', this.state.comments);	
                formData.append('image_user', userData.user.auth.avatar);	
                formData.append('name_user', userData.user.auth.name);
                let url = 'http://api-local.com/api/blog/comment/' + this.props.idBlog;
                
                
                axios.post(url, formData, config)	
                .then(res => {	
                    this.props.loadComment(res.data.data);
                    console.log(res);
                    alert('bạn comment thành công')

                    this.setState({
                        comments : ''
                    })
                })
                .catch(error => {		
                    console.log(error)		
                })
        }
   
    }

    render(){
        
        return(
            <> 
                <div className="replay-box">
                    <div className="row">
                        <div className="col-sm-4">
                        {/* level replay */}
                        </div>
                        <div className="col-sm-8">
                        <div className="text-area">
                            <div className="blank-arrow">
                                <label>Your Name</label>
                                {/* <Formerrors formErrors ={this.state.formErrors}/> */}
                            </div>
                            <form  onSubmit={this.handleSubmitComment}>
                                <span>*</span>
                                <textarea value={this.state.comments}  name="message" type="text" placeholder="new comment" rows={11}   onChange={this.handleComment}  /> 
                                <button className="btn btn-primary" type="submit" >post comment</button>
                            </form>
                        </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
export default Comment;