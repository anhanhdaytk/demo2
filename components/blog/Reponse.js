import React, { Component } from 'react';
import { Link } from 'react-router-dom';
class Reponse extends Component{
    render() {
        return(
            <div className="response-area">
                <h2>3 RESPONSES</h2>
                <ul className="media-list">
                    <li className="media">
                    <Link className="pull-left" to="/">
                        <img className="media-object" src="images/blog/man-two.jpg" alt="" />
                    </Link>
                    <div className="media-body">
                        <ul className="sinlge-post-meta">
                        <li><i className="fa fa-user" />Janis Gallagher</li>
                        <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                        <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                        </ul>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <Link className="btn btn-primary" to="/"><i className="fa fa-reply" />Replay</Link>
                    </div>
                    </li>
                    <li className="media second-media">
                    <Link className="pull-left" to="/">
                        <img className="media-object" src="images/blog/man-three.jpg" alt="" />
                    </Link>
                    <div className="media-body">
                        <ul className="sinlge-post-meta">
                        <li><i className="fa fa-user" />Janis Gallagher</li>
                        <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                        <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                        </ul>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <Link className="btn btn-primary" to="/"><i className="fa fa-reply" />Replay</Link>
                    </div>
                    </li>
                    <li className="media">
                    <Link className="pull-left" to="/">
                        <img className="media-object" src="images/blog/man-four.jpg" alt="" />
                    </Link>
                    <div className="media-body">
                        <ul className="sinlge-post-meta">
                        <li><i className="fa fa-user" />Janis Gallagher</li>
                        <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                        <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                        </ul>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <Link className="btn btn-primary" to="/"><i className="fa fa-reply" />Replay</Link>
                    </div>
                    </li>
                </ul>					
            </div>

        )
    }
}
export default Reponse;