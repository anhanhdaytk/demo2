import React, { Component } from 'react';
import StarRatings from 'react-star-ratings';
import axios from 'axios';
class Rate extends Component{
    constructor(props){
        super(props)
        this.state = {
            rating: 0,
           
        }
        this.changeRating = this.changeRating.bind(this);
        // this.handleTbc = this.handleTbc.bind(this);
    }
    
    changeRating(newRating, name){
        this.setState({
            rating : newRating
        })
        
        // 
        
        
        const userData = JSON.parse(localStorage["appState"])
        let accessToken = userData.user.auth_token;
        // let userData = JSON.parse(localStorage.getItem('rate'))		
        // console.log(userData);	
        // let accessToken = userData.success.token;
        

        console.log(userData.user.auth.id);
        
        
        console.log(this.props.idBlog)
        
        console.log(accessToken);
        
        let config = { 
            headers: { 
            'Authorization': 'Bearer '+ accessToken,
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
            } 
        };
        let formData = new FormData(); 	
        formData.append('user_id', userData.user.auth.id);	 
        formData.append('blog_id', this.props.idBlog);
        formData.append('rate', newRating);		
              
        let url = 'http://api-local.com/api/blog/rate/' + this.props.idBlog;
              
        axios.post(url, formData, config)		
            .then(res => {		
                alert(res.data.message)		
             })		
            .catch(error => {		
                console.log(error)		
            })	
            
    }
    
    componentDidMount(){
        axios.get('http://api-local.com/api/blog/rate/' + this.props.idBlog)
        .then(res => {
           
            
            
            
            
            if(res.data.data) {
                //console.log('có dữ liệu');
                
                let totalRating = 0;
                Object.keys(res.data.data).map((key,i) => {
                    totalRating += res.data.data[key].rate
                    //console.log(res);
                    console.log(res.data.data.length);
                })
                console.log(Object.keys(res.data.data).length)
                this.setState({
                    rating : totalRating/Object.keys(res.data.data).length
                })
                
            }
           
        })
        
    }
   
    render(){
            

        return(
            
            <StarRatings
            rating={this.state.rating}
            starRatedColor="yellow"
            changeRating={this.changeRating}
            numberOfStars={5}
            name='rating'
            />
            
        )
    }
}
export default Rate;