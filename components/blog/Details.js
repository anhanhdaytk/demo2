
import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import Rate from './Rate';
import ListComment from './ListComment';

import Comment from './Comment';
// import Rate from './Rate';
// import Reponse from './Reponse';
class Details extends Component{
    constructor(props){
        super(props)
        this.state = {
            details : [],
            listComments : [],
            
        }
        this.loadComment = this.loadComment.bind(this);
        this.handleSubComment = this.handleSubComment.bind(this);
    }
    componentDidMount (){
        axios.get('http://api-local.com/api/blog/detail/' + this.props.match.params.id)
            .then(res => {
                
                //this.setState({ blogs: res.data });
               console.log(res.data.data)
               
                this.setState({ 
                    details: res.data.data,
                    listComments: res.data.data.comment,
                });
            })
            .catch  (error => console.log(error));
              
    }
    
    loadComment(b){
        this.setState({listComments: this.state.listComments.concat(b)})
    }
    handleSubComment(s){
        
        this.setState({idSubComment: s})
        console.log(s)
    }
    fecthData() {
        let details = this.state.details
       
        //let listComments = this.state.listComments
        // Object.keys(details).map((key,index) =>{})
        //console.log(listComments);
          
        return(
            
            <section>
                <div className="container">
                    <div className="row">
                        <div className="col-sm-9" >
                            <div className="blog-post-area">
                                <h2 className="title text-center">LATEST FROM OUR BLOG</h2>
                                <div className="single-blog-post">
                                    <h3>{details.title}</h3>
                                    <div className="post-meta">
                                        <ul>
                                            <li><i className="fa fa-user" /> Mac Doe</li>
                                            <li><i className="fa fa-clock-o" />{details.created_at}</li>
                                            <li><i className="fa fa-calendar" /> {details.update_at}</li>
                                        </ul>
                                        <span>
                                            <Rate idBlog = {this.props.match.params.id}/>  
                                        </span>
                                    </div>
                                    <Link to="/">
                                        <img src={"http://api-local.com/upload/Blog/image/" + details.image} alt="" />
                                    </Link>
                                    <p>{details.content}</p>
                                    
                                    <div className="pager-area">
                                        <ul className="pager pull-right">
                                            <li><Link to="/">Pre</Link></li>
                                            <li><Link to="/">Next</Link></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <ListComment 
                                listComments ={this.state.listComments}
                                handleSubComment = {this.handleSubComment}
                            />
                                        
                            <Comment 
                                idBlog = {this.props.match.params.id}
                                idSubComment = {this.state.idSubComment}
                                loadComment={this.loadComment}
                            />
                            
                        </div>
                    </div>
                </div>

                

                

            </section>
                
        )
    }   // fetchdata 
   
    render(){
        // console.log(this.state.listComments);
        return (
            <div>{this.fecthData()}</div>
        )
    }
}
export default Details;