import React, { Component } from 'react';
import axios from 'axios';

import { Link } from 'react-router-dom';

class Blog extends Component{
    constructor(props){
        super(props)
        this.state = {
            blogs : []    
        }
        
    }
    componentDidMount (){
        axios.get('http://api-local.com/api/blog')
            .then(res => {
                // const blogs = res.data;
                //this.setState({ blogs: res.data });
                //console.log(res.data.blog.data);
                this.setState({ 
                    blogs: res.data.blog.data
                });
            })
            .catch  (error => console.log(error));
            
    }
    fecthData() {
        return this.state.blogs.map((value,key) =>  {
          
            return(
                <section key={key}>
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-9" >
                                <div className="blog-post-area">
                                    <h2 className="title text-center">LATEST FROM OUR BLOG</h2>
                                    <div className="single-blog-post">
                                        {/* {this.state.blog.map((key,value) => <h1>{key['title']}</h1>)} */}
                                        <h3>{value['title']}</h3>
                                        
                                        <div className="post-meta">
                                            <ul>
                                                <li><i className="fa fa-user" /> Mac Doe</li>
                                                <li><i className="fa fa-clock-o" /> 1:33 pm</li>
                                                <li><i className="fa fa-calendar" /> DEC 5, 2013</li>
                                            </ul>
                                            <span>
                                                   
                                                {/* <i className="fa fa-star" />
                                                <i className="fa fa-star" />
                                                <i className="fa fa-star" />
                                                <i className="fa fa-star" />
                                                <i className="fa fa-star-half-o" /> */}
                                            </span>
                                        </div>
                                        <a>
                                            <img src={"http://api-local.com/upload/Blog/image/" + value['image']} alt="" />
                                        </a>
                                        <p>{value['description']}</p>
                                        {/* <p>{value['content']}</p> */}
                                        <Link to={"/blog/detail/"+ value['id']}  className="btn btn-primary">Read More</Link>
                                    </div>
                                    
                                    
                                    {/* <div className="pagination-area">
                                        <ul className="pagination">
                                            <li><a href className="active">1</a></li>
                                            <li><a href>2</a></li>
                                            <li><a href>3</a></li>
                                            <li><a href><i className="fa fa-angle-double-right" /></a></li>
                                        </ul>
                                    </div> */}
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                    
            )
            
      })
    }    
   
    render(){
    //     const {blogs} = this.state;
    //     return(
    //     <div>
    //       {
    //         blogs.lenght ? blogs.map(blogs => <div key={blogs.id}>{blogs.data.blog.title}</div>):null

    //       }
    //     </div>
    //    ) 
   
    
       return (
       <div>{this.fecthData()}</div>
       )
   }
}
export default Blog;