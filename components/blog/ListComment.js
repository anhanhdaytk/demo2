import React, { Component } from 'react';
import { Link } from 'react-router-dom';


class ListComment extends Component{
    constructor(props){
        super(props)
        this.handleSubComment = this.handleSubComment.bind(this);
    }
    handleSubComment(e){
        // this.setState({
        //     idSubComment: e.target.id
        // })
        this.props.handleSubComment(e.target.id);
        //console.log(e.target.id);
        
    }
    renderListCmt() {
        let listComments = this.props.listComments
        
        return Object.keys(listComments).map((key, i) =>{ 
           
            
            if(listComments[key].id_comment === 0) {
                return (
                    
                    <>
                        <li className="media">
                            <Link className="pull-left" to="/">
                                <img className="media-object" src={"http://api-local.com/upload/user/avatar/" + listComments[key].image_user} alt="" />
                            </Link>
                            <div className="media-body">
                                <ul className="sinlge-post-meta">
                                    <li><i className="fa fa-user" />{listComments[key].name_user}</li>
                                    <li><i className="fa fa-clock-o" />{listComments[key].created_at}</li>
                                    {/* <li><i className="fa fa-calendar" /> DEC 5, 2013</li> */}
                                </ul>
                                <p>{listComments[key].comment}</p>
                                <a  className="btn btn-primary" onClick={this.handleSubComment} id={listComments[key].id}><i className="fa fa-reply" />Replay</a>
                                {/* replay */}
                                {/* <form  onSubmit={this.handleSubmit} >
                                    <textarea name="message" type="text" placeholder="new comment"  onChange={this.handleSubComment} value={this.state.subComments} />
                                    <button className="btn btn-primary" type="submit" >post comment</button>
                                </form> */}
                            </div>
                        </li>

                        {
                            Object.keys(listComments).map((key2, i2) =>{
                                
                                if(listComments[key2].id_comment === listComments[key].id){
                                
                                    return(
                                        
                                            <li key={key2} className="media second-media">
                                                <Link className="pull-left" to="/">
                                                    <img className="media-object" src={"http://api-local.com/upload/user/avatar/" + listComments[key2].image_user} alt="" />
                                                </Link>
                                                <div className="media-body">
                                                    <ul className="sinlge-post-meta">
                                                        <li><i className="fa fa-user" />{listComments[key2].name_user}</li>
                                                        <li><i className="fa fa-clock-o" />{listComments[key2].created_at}</li>
                                                        {/* <li><i className="fa fa-calendar" /> DEC 5, 2013</li> */}
                                                    </ul>
                                                    <p>{listComments[key2].comment}</p>
                                                    {/* <a className="btn btn-primary" onChange={this.handleSubComment} id={listComments[key2].id}><i className="fa fa-reply" />Replay</a> */}
                                                </div>
                                            </li>
                                    
                                    )
                                }

                            })

                        }
                       
                    </>

                )
                  
            }         
        })
        
    }
    
    render(){
       
        //console.log();
        return(
                <div className="media commnets listComments">
                    <div className="response-area">
                        {/* <h2>3 RESPONSES</h2> */}
                        <ul className="media-list">
                            {this.renderListCmt()}
                        </ul>
                    </div>           
                    
                </div>
                        
                                        
                                    				
                                

                                /* <Link to="/" className="pull-left" >
                                <img className="media-object" src="images/blog/man-one.jpg" alt="" />
                                </Link>
                                <div className="media-body">
                                    <h4 className="media-heading">{listComments[key].name_user}</h4>
                                    <p>{listComments[key].comment}</p>
                                    <div className="blog-socials">
                                        <ul>
                                            <li><Link to="/"><i className="fa fa-facebook" /></Link></li>
                                            <li><Link to="/"><i className="fa fa-twitter" /></Link></li>
                                            <li><Link to="/"><i className="fa fa-dribbble" /></Link></li>
                                            <li><Link to="/"><i className="fa fa-google-plus" /></Link></li>
                                        </ul>
                                        <Link to="/" className="btn btn-primary" >Other Posts</Link>
                                    </div>
                                </div> */
                           
                    
                
        )
   }
}
export default ListComment;